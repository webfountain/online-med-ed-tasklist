import React, { useState } from 'react';
import Calendar from 'react-calendar';

import {
  AddCalendar as AddCalendarIcon,
  SuccessCalendar as SuccessCalendarIcon
} from 'Elements/Icons';
import {
  editTaskForm, fieldsetForm, fieldsetSections,
  calendarCompletionDate, completionDateStyle, taskCompletedStyle,
  editTaskButton, cancelEditTaskButton, noTaskToEdit
} from './styles.css';


function EditCalendarWrapper({ completionDate, setCompletionDate }) {
  const [isCalendarOpen, setIsCalendarOpen] = useState(false);
  const [dueDate, setDueDate] = useState(new Date());

  function handleCalendarClick(e) {
    if (e.currentTarget.childNodes[0].className === 'react-calendar') return;

    setIsCalendarOpen(true);
  }

  function handleClickDay(value, e) {
    e.preventDefault();
    setIsCalendarOpen(false);
    setCompletionDate(value.toLocaleDateString('en-US'));
    setDueDate(value);
  }

  return (
    <div className={calendarCompletionDate}
      onClick={handleCalendarClick}>
      {!isCalendarOpen && completionDate &&
        <span className={completionDateStyle}>{`${completionDate}`} <SuccessCalendarIcon /></span>
      }
      {!isCalendarOpen && !completionDate &&
        <span className={completionDateStyle}>{'Select a Date:'} <AddCalendarIcon /></span>
      }
      {isCalendarOpen &&
        <Calendar
          onClickDay={handleClickDay}
          minDate={new Date()}
          value={dueDate} />
      }
    </div>
  );
}

const EditTaskPanel = ({ taskToEdit, setTaskToEdit, setEditTask, setPanel }) => {
  const [taskName, setTaskName] = useState(taskToEdit.title);
  const [taskDescription, setTaskDescription] = useState(taskToEdit.description);
  const [taskCompleted, setTaskCompleted] = useState(taskToEdit.completed);
  const [completionDate, setCompletionDate] = useState(taskToEdit.completionDate);

  function handleSubmit(e) {
    e.preventDefault();

    setEditTask({ ...taskToEdit,
      title: taskName,
      description: taskDescription,
      completed: taskCompleted,
      completionDate: completionDate
    });
    setTaskToEdit({});
  }

  function handleCancelClick() {
    setTaskToEdit({});
    setPanel('allTasks');
  }

  return (
    <>
      {!taskToEdit.id
        ? <div className={noTaskToEdit}>
            <p>Select a task to edit in the <span>Task List</span> section</p>
          </div>
        : <form id="editTaskForm"
          className={editTaskForm}
          onSubmit={handleSubmit}>
          <div className={fieldsetSections}>
            <fieldset name="taskInformation"
              className={fieldsetForm}
              form="editTaskForm">

              <label htmlFor="taskName">
                <h3>Task Name</h3>
                <input type="text"
                  id="taskName"
                  name="taskName"
                  value={taskName}
                  onChange={(e) => setTaskName(e.target.value)}
                  required
                />
              </label>

              <label htmlFor="taskDescription">
                <h3>Description</h3>
                <textarea
                  id="taskDescription"
                  name="taskDescription"
                  value={taskDescription}
                  onChange={(e) => setTaskDescription(e.target.value)}
                  required
                />
              </label>

              <div>
                <h3>Target Completion Date</h3>
                  <EditCalendarWrapper
                    completionDate={completionDate}
                    setCompletionDate={setCompletionDate} />

              </div>
            </fieldset>
            <fieldset className={fieldsetForm}>
              <div className={taskCompletedStyle}>
                {taskCompleted
                  ? <label htmlFor="isCompleted">Completed</label>
                  : <label htmlFor="isCompleted">Is Completed?</label>
                }
                <input id="isCompleted"
                  type="checkbox"
                  name="isCompleted"
                  value={taskCompleted}
                  onChange={() => setTaskCompleted(!taskCompleted)}
                  checked={taskCompleted}
                  />
              </div>
            </fieldset>
          </div>

          <div>
            <button className={editTaskButton}
              type="submit" label="Save Task" form="editTaskForm">
              Save
            </button>
            <button className={cancelEditTaskButton}
              type="button" label="Cancel Edit Task"
              onClick={handleCancelClick}>
              Cancel
            </button>
          </div>
        </form>
      }
    </>
  );
};


export default EditTaskPanel;
