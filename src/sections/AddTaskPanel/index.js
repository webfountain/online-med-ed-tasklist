import React, { useState } from 'react';
import Calendar from 'react-calendar';

import {
  AddCalendar as AddCalendarIcon,
  SuccessCalendar as SuccessCalendarIcon
} from 'Elements/Icons';
import {
  addTaskForm, fieldsetForm, completionDate,
  calendarCompletionDate, addTaskButton
} from './styles.css';


function randomNumberGenerator() {
  const min = 10000;
  const max = 99999;

  return Math.floor(
    Math.random() * (max - min) + min
  )
}

function AddCalendarWrapper({ completionDate, setCompletionDate }) {
  const [isCalendarOpen, setIsCalendarOpen] = useState(false);
  const [dateSelected, setDateSelected] = useState(false);
  const [dueDate, setDueDate] = useState(new Date());

  function handleCalendarClick(e) {
    if (e.currentTarget.childNodes[0].className === 'react-calendar') return;
    if (dateSelected) setDateSelected(false);

    setIsCalendarOpen(true);
  }

  function handleClickDay(value, e) {
    e.preventDefault();
    setIsCalendarOpen(false);
    setCompletionDate(value.toLocaleDateString('en-US'));
    setDueDate(value);
    setDateSelected(true);
  }

  return (
    <div className={calendarCompletionDate}
      onClick={handleCalendarClick}>
      {!isCalendarOpen && !dateSelected
        ? <span>Select a Date: <AddCalendarIcon /></span>
        : !isCalendarOpen && dateSelected
          ? <span className={completionDate}>{`${completionDate}`} <SuccessCalendarIcon /></span>
          : <Calendar
              onClickDay={handleClickDay}
              minDate={new Date()}
              value={dueDate} />
      }
    </div>
  );
}

const AddTaskPanel = ({ addNewTask }) => {
  const [taskName, setTaskName] = useState('');
  const [taskDescription, setTaskDescription] = useState('');
  const [completionDate, setCompletionDate] = useState();
  const [completionDateSelected, setCompletionDateSelected] = useState(false);

  function handleSubmit(e) {
    e.preventDefault();

    addNewTask({
      id: randomNumberGenerator(),
      title: taskName,
      description: taskDescription,
      completed: false,
      completionDate: completionDate
    });
  }

  return (
    <>
      <form id="addTaskForm"
        className={addTaskForm}
        onSubmit={handleSubmit}>
        <fieldset name="taskInformation"
          className={fieldsetForm}
          form="addTaskForm">

          <label htmlFor="taskName">
            <h3>Task Name</h3>
            <input type="text"
              id="taskName"
              name="taskName"
              value={taskName}
              onChange={(e) => setTaskName(e.target.value)}
              required
            />
          </label>

          <label htmlFor="taskDescription">
            <h3>Description</h3>
            <textarea
              id="taskDescription"
              name="taskDescription"
              value={taskDescription}
              onChange={(e) => setTaskDescription(e.target.value)}
              required
            />
          </label>

          <div>
            <h3>Target Completion Date</h3>
            {!completionDateSelected
              ? <AddCalendarWrapper completionDate={completionDate} setCompletionDate={setCompletionDate} />
              : <SuccessCalendar />
            }

          </div>
        </fieldset>

        <button className={addTaskButton}
          type="submit" label="Add Task" form="addTaskForm">
          Add Task
        </button>
      </form>
    </>
  );
};


export default AddTaskPanel;
