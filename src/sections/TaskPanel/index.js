import React from 'react';
import {
  AddTask as AddTaskIcon,
  EditTask as EditTaskIcon,
  TaskList as TaskListIcon,
} from 'Elements/Icons';

import { taskList, taskListItem, active } from './styles.css';


const panelMap = {
  'Task List': 'allTasks',
  'Add Task': 'addTask',
  'Edit Task': 'editTask'
};

const TaskPanel = ({ panel, setPanel }) => {
  function handleClick(e) {
    const panel = e.currentTarget.childNodes[1].textContent;
    setPanel(panelMap[panel]);
  }

  return (
    <ul className={taskList}>
      <li
        className={`${taskListItem} ${panel === 'allTasks' ? active : null}`}
        onClick={handleClick}>
        <TaskListIcon />
        <span>Task List</span>
      </li>
      <li
        className={`${taskListItem} ${panel === 'addTask' ? active : null}`}
        onClick={handleClick}>
        <AddTaskIcon />
        <span>Add Task</span>
      </li>
      <li
        className={`${taskListItem} ${panel === 'editTask' ? active : null}`}
        onClick={handleClick}>
        <EditTaskIcon />
        <span>Edit Task</span>
      </li>
      {/* <li>Completed Task</li> */}
    </ul>
  );
};


export default TaskPanel;
