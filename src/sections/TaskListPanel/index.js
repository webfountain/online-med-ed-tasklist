import React from 'react';

import { Accordion, AccordionItem } from 'react-sanfona';

import {
  allTasks, taskInfo, taskTitle,
  taskDescription, taskCompletionDate,
  taskCompleted, taskActionButtons,
  taskItem, bodyTaskItem, expandedTaskItem
} from './styles.css';


const TaskListPanel = ({ taskList, setTaskList, editTask, deleteTask, updateTask }) => {
  function handleCheckBox(e) {
    const taskId = e.target.value;
    taskList.forEach(task => {
      if (task.id == taskId) task.completed = !task.completed
    });
    setTaskList([...taskList]);
    updateTask(JSON.parse(e.target.value));
  }

  function handleEditClick(e) {
    e.preventDefault();
    editTask(JSON.parse(e.target.value));
  }

  return (
    <>
      <ul className={allTasks}>
        <Accordion style={{ backgroundColor: 'white' }} allowMultiple='true'>
          {taskList.map(task => {
            return (
              <AccordionItem
                className={taskItem}
                titleClassName={taskTitle}
                bodyClassName={bodyTaskItem}
                expandedClassName={expandedTaskItem}
                key={task.id} title={`${task.title}`}>

              <div className={taskInfo}>
                <section>
                  <div className={taskDescription}>
                    <h4>Description</h4>
                    <p>{task.description}</p>
                  </div>

                  <div className={taskCompletionDate}>
                    <h4>Target Completion Date</h4>
                    <p>{`${task.completionDate ? task.completionDate : 'No date set'}`}</p>
                  </div>
                </section>
                <section>
                  <div className={taskCompleted}>
                    {task.completed
                      ? <label htmlFor="isCompleted">Completed</label>
                      : <label htmlFor="isCompleted">Is Completed?</label>
                    }
                    <input id="isCompleted"
                      type="checkbox"
                      name="isCompleted"
                      value={task.id}
                      onChange={handleCheckBox}
                      checked={task.completed}
                      />
                  </div>
                  <div className={taskActionButtons}>
                    <button
                      onClick={handleEditClick}
                      value={`${JSON.stringify(task)}`}>
                        Edit Task
                    </button>
                    <button
                      onClick={(e) => deleteTask(JSON.parse(e.target.value))}
                      value={`${JSON.stringify(task)}`}>
                        Delete Task
                    </button>
                  </div>
                </section>
              </div>

            </AccordionItem>
            );
          })}
        </Accordion>
      </ul>
    </>
  );
};


export default TaskListPanel;
