import React, { useState } from 'react';

import TaskListPanel from '../TaskListPanel';
import AddTaskPanel from '../AddTaskPanel';
import EditTaskPanel from '../EditTaskPanel';
import { mainPanel, mainTitle } from './styles.css';


const panelMap = {
  'allTasks': 'Task List',
  'addTask': 'Add Task',
  'editTask': 'Edit Task'
};

const MainPanel = ({
  panel, setPanel, taskList,
  setTaskList, addNewTask, setEditTask,
  deleteTask, updateTask
}) => {
  const [taskToEdit, setTaskToEdit] = useState({});

  function editTask(task) {
    setTaskToEdit(task);
    setPanel('editTask');
  }

  return (
    <div className={mainPanel}>
      <header className={mainTitle}>
        <h2>{panelMap[panel]}</h2>
      </header>
      {panel === 'allTasks'
        ? <TaskListPanel
            taskList={taskList}
            setTaskList={setTaskList}
            editTask={editTask}
            deleteTask={deleteTask}
            updateTask={updateTask} />
        : panel === 'addTask'
          ? <AddTaskPanel addNewTask={addNewTask} />
          : <EditTaskPanel
              taskToEdit={taskToEdit}
              setTaskToEdit={setTaskToEdit}
              setEditTask={setEditTask}
              setPanel={setPanel} />
      }
    </div>
  );
};


export default MainPanel;
