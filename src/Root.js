import { hot } from 'react-hot-loader/root';
import React, { useEffect, useState } from 'react';

import TaskPanel from 'Sections/TaskPanel';
import MainPanel from 'Sections/MainPanel';
import { taskPanel } from './Root.css';


const Root = () => {
  const [panel, setPanel] = useState('allTasks');
  const [taskList, setTaskList] = useState([]);

  async function addNewTask(task) {
    const newTaskList = [...taskList, task];
    setTaskList(newTaskList);

    const res = await fetch('/.netlify/functions/tasks', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(task)
    });

    console.log(res.headers);
    setPanel('allTasks');
  }

  async function setEditTask(task) {
    const newTaskList = taskList.filter(_task => _task.id !== task.id);
    setTaskList([...newTaskList, task]);

    const res = await fetch(`/.netlify/functions/tasks/${task.id}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(task)
    });
    const json = await res.json();
    setPanel('allTasks');
  }

  async function updateTask(task) {
    const res = await fetch(`/.netlify/functions/tasks/${task.id}`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(task)
    });
  }

  async function deleteTask(task) {
    const newTaskList = taskList.filter(_task => _task.id !== task.id);
    setTaskList([...newTaskList ]);
    await fetch(`/.netlify/functions/tasks/${task.id}`, { method: 'DELETE' });
  }

  useEffect(() => {
    const fetchData = async () => {
      const res = await fetch('/.netlify/functions/tasks');
      const json = await res.json();
      setTaskList(json.data);
    };

    fetchData();
  }, []);

  return (
    <>
      <header style={{  gridArea: '1 / 2 / 2 / 3' }}>
        <h1>TASKS</h1>
      </header>

      <main style={{ gridArea: '2 / 2 / 4 / 3' }} className="main-content">
        <MainPanel
          panel={panel}
          setPanel={setPanel}
          taskList={taskList}
          setTaskList={setTaskList}
          setEditTask={setEditTask}
          addNewTask={addNewTask}
          deleteTask={deleteTask}
          updateTask={updateTask} />
      </main>

      <aside style={{ gridArea: '2 / 1 / 4 / 2' }} className={taskPanel}>
        <TaskPanel panel={panel} setPanel={setPanel} />
      </aside>
    </>
  );
};


export default hot(Root);
