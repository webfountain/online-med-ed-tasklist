const mockData = [
  {
    id: 12345,
    title: 'check history of patient 12345',
    description: `heart disease & disabetes. ask if it runs in the family`,
    completed: true,
    completionDate: new Date().toLocaleDateString('en-US')
  },
  {
    id: 23456,
    title: 'study for exam',
    description: 'Mauris quis quam in lacus tempor lacinia vitae eget ex. In rhoncus eleifend augue ultrices varius. Proin faucibus elementum suscipit. In hac habitasse platea dictumst. Curabitur elementum luctus risus, ut porta ante pellentesque eu. Integer aliquam nisl magna, at finibus libero malesuada vitae. Aenean sit amet blandit justo. Curabitur dignissim metus eros, non gravida ipsum fermentum eget. Praesent faucibus pellentesque tortor sed rhoncus. Suspendisse ligula mauris, vehicula vitae lacinia ut, imperdiet vel risus. Fusce pellentesque ligula ac tortor elementum viverra. Suspendisse porta posuere lacus, a rutrum turpis interdum sed. ',
    completed: false,
    completionDate: new Date().toLocaleDateString('en-US')
  },
  {
    id: 34567,
    title: 'call doctor for follow up',
    description: 'questions regarding patient 12345 -- mentioned he would check notes',
    completed: true,
    completionDate: new Date().toLocaleDateString('en-US')
  }
];

let taskList = [...mockData];


exports.handler = async (event, context) => {
  const { path, httpMethod, body } = event;
  let resBody = {};

  if (httpMethod === 'GET') {
    resBody = {
      statusCode: 200,
      body: JSON.stringify({ data: taskList })
     }
  }
  if (httpMethod === 'POST') {
    const data = JSON.parse(body);
    taskList = [...taskList, data];
    resBody = {
      statusCode: 201,
      headers: {
        'Location': `/tasks/${data.id}`
      }
    }
  }
  if (httpMethod === 'PUT') {
    const data = JSON.parse(body);
    const pathArr = path.split('/');
    const id = pathArr[pathArr.length-1];
    const updatedTaskList = taskList.map(task => {
      if (task.id == id) return data;
      return task;
    });
    taskList = [...updatedTaskList];

    resBody = {
      statusCode: 200,
      body: JSON.stringify({ data: taskList })
    }
  }
  if (httpMethod === 'DELETE') {
    const pathArr = path.split('/');
    const id = pathArr[pathArr.length-1];
    const updatedTaskList = taskList.filter(task => task.id != id);
    taskList = [...updatedTaskList];

    resBody = {
      statusCode: 200,
      body: JSON.stringify({ data: taskList })
    }
  }

  return resBody;
}
